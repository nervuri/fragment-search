// SPDX-License-Identifier: CC0-1.0
'use strict'

// Insert "Link to here" button
document.getElementsByTagName('body')[0].insertAdjacentHTML('beforeend', '<button id="btn">🔗 Link to here</button>')
const linkToHereButton = document.getElementById('btn')

/*
 * Display "link to here" button when text is selected
 */
function textSelected (event) {
  const selectedText = window.getSelection().toString()

  if (selectedText.length === 0) {
    linkToHereButton.style.display = 'none'
    return
  }

  linkToHereButton.style.display = 'block'

  const x = event.pageX
  const y = event.pageY
  const linkToHereButtonWidth = Number(window.getComputedStyle(linkToHereButton).width.slice(0, -2))
  const linkToHereButtonHeight = Number(window.getComputedStyle(linkToHereButton).height.slice(0, -2))

  linkToHereButton.style.left = (x - linkToHereButtonWidth * 0.5) + 'px'
  linkToHereButton.style.top = (y - linkToHereButtonHeight * 1.75) + 'px'
}

/*
 * Generate link to selected text
 */
function generateURL () {
  const selectedText = window.getSelection().toString()

  // Get occurrence number
  let occurrence = 1
  let index = 0
  let startIndex = 0
  const offset = Math.min(window.getSelection().anchorOffset, window.getSelection().focusOffset)
  const text = document.getElementById('text').innerText
  const textBeforeSelection = text.substr(0, offset)
  while ((index = textBeforeSelection.indexOf(selectedText, startIndex)) !== -1) {
    ++occurrence
    startIndex = index + selectedText.length
  }

  // Build URL
  const l = window.location
  let url = l.protocol + '//' + l.host + l.pathname + l.search
  url += '#' + encodeURIComponent(selectedText)
  if (occurrence !== 1) {
    url += ':' + occurrence
  }

  // Copy URL to clipboard
  if (navigator.clipboard) {
    navigator.clipboard.writeText(url)
  }

  // Open the URL
  window.location.href = url

  // Hide "Link to here" button
  linkToHereButton.style.display = 'none'
}

// ========================

// Event listeners
linkToHereButton.addEventListener('click', generateURL)
document.getElementById('text').addEventListener('mouseup', textSelected)
document.addEventListener('mousedown', function (event) {
  if (event.target !== linkToHereButton) {
    linkToHereButton.style.display = 'none'
  }
})
