// SPDX-License-Identifier: CC0-1.0
'use strict'

function fragmentSearch () {
  const fragment = window.location.hash.substr(1)
  if (fragment === '') {
    return
  }
  let searchString = decodeURIComponent(fragment)
  let occurrence = 1 // nth occurrence of searchString
  const caseSensitive = true
  let backwardSearch = false

  // Check if [:occurrence] is specified
  const match = fragment.match(/^(.+?):(-?\d+)$/)
  if (match) {
    occurrence = Number(match[2])
    searchString = decodeURIComponent(match[1])
    if (occurrence === 0) {
      // Highlight all (not implemented)
      // and go to first result
      occurrence = 1
    } else if (occurrence < 0) {
      backwardSearch = true
      occurrence = -occurrence
    }
  }

  // Count all occurrences of searchString in page
  const allText = document.getElementsByTagName('body')[0].innerText
  const allOccurrences = allText.split(searchString).length - 1

  // Reset window.find() to the top of the page
  const searchStringWithHtmlEntities = searchString.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;')
  document.getElementsByTagName('body')[0]
    .insertAdjacentHTML('afterbegin', '<div id="fragment-search-top">' + searchStringWithHtmlEntities + '</div>')
  for (let i = 0; i < allOccurrences + 1; ++i) {
    window.find(searchString, caseSensitive, true) // backward search
  }
  document.getElementById('fragment-search-top').remove()

  // Skip elements with class ".skip"
  const elementsToSkip = document.getElementsByClassName('skip')
  let occurrencesToSkip = 0
  for (const el of elementsToSkip) {
    const occurrencesInElement = el.innerText.split(searchString).length - 1
    occurrencesToSkip += occurrencesInElement
  }
  if (occurrencesToSkip === allOccurrences) {
    return
  }
  occurrence += occurrencesToSkip

  // Search
  for (let i = 0; i < occurrence; ++i) {
    window.find(searchString, caseSensitive, backwardSearch)
  }
}

// ========================

// Run search on page load
fragmentSearch()

// Run search when the fragment changes
window.addEventListener('hashchange', function (e) {
  fragmentSearch()
})
