<!DOCTYPE html>
<html lang="en">
<!-- SPDX-License-Identifier: CC0-1.0 -->

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="theme-color" content="#000">
<meta name="referrer" content="no-referrer">

<title>URI fragment search</title>

<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="extra.css">
<link rel="stylesheet" href="button.css">

<main>
<section id="explainer" class="skip">
<div class="center">
	<center>
		<h1><a href="">URI fragment search</a></h1>
	</center>
</div>
<p>
Demo for using the URI fragment (the part after "#") as input for searching text in the current page. The fragment's syntax is: <code>#&lt;text&gt;[:&lt;occurrence&gt;]</code> - where &lt;text&gt; is a URI-encoded excerpt from the document and &lt;occurrence&gt; is an optional integer.
</p>
<noscript><p style="color:red">It only works with JavaScript enabled.</p></noscript>
<p>
Examples:<br>
<ul>
	<li><a href="#document">basic search</a></li>
	<li><a href="#document:2">2nd occurrence</a></li>
	<li><a href="#%23%23%20Links%0A%0ALike%20Gopher">including encoded chars: #, space, newline</a></li>
	<li><a href="#Anything%20which%20comes%20after%20the%20%60%60%60%20characters%20of%20a%20line%20which%20toggles%20preformatted%20line%20*on*%20(i.e.%20the%20first%2C%20third%2C%20fifth%2C%20etc.%20toggling%20lines%20in%20a%20document)%20may%20be%20treated%20as%20%22alt%20text%22%20for%20the%20preformatted%20content.%20%20In%20general%20you%20should%20not%20count%20on%20this%20content%20being%20visible%20to%20the%20user%20but%2C%20for%20example%2C%20search%20engines%20may%20index%20it%20and%20screen%20readers%20may%20read%20it%20to%20users%20to%20help%20the%20user%20decide%20whether%20the%20preformatted%20content%20should%20be%20read%20aloud%20(which%20e.g.%20ASCII%20art%20generally%20should%20not%20be%2C%20but%20which%20source%20code%20perhaps%20should%20be).%20%20There%20are%20currently%20no%20established%20conventions%20on%20how%20alt%20text%20should%20be%20formatted.">highlight final paragraph</a></li>
</ul>
</p>
<p>Select text from below the line to generate new searches.</p>
<p>
<a href="https://gitlab.com/nervuri/fragment-search">The source code</a> is released under <a href="https://creativecommons.org/publicdomain/zero/1.0/">CC0 (public domain)</a>.
</p>
<p>
Text source:<br>
<a href="gemini://gemini.circumlunar.space/docs/gemtext.gmi">gemini://gemini.circumlunar.space/docs/gemtext.gmi</a>
[ <a href="https://gemini.circumlunar.space/">About Gemini</a> ]<br>
</p>
</section>

<hr>
<pre id="text">
# A quick introduction to "gemtext" markup

The most common way to serve textual content over Gemini is not just plain text like on Gopher, but using a lightweight markup language called "Gemtext" (which is served with the unofficial MIME type text/gemini).  This document is a quick introduction to that markup language.  It has some superficial resemblences to Markdown, which will make it easy to learn if you know MD, but it's quite different in other ways.

Once you've read this document, you might like to occasionally refresh your memory by refering to the:

=&gt; cheatsheet.gmi Gemtext cheatsheet

## Text

Text in Gemtext documents is written using "long lines", i.e. you (or your editor) shouldn't be inserting newline characters every 80 characters or so.  Instead, leave it up to the receiving Gemini client to wrap your lines to fit the device's screen size and the user's preference.  This way Gemtext content looks good and is easy to read on desktop monitors, laptop screens, tablets and smartphones.

Note that while Gemini clients will break up lines of text which are longer than the user's screen, they will not join up lines which are shorter than the user's screen, like would happen in Markdown, HTML or LaTeX.  This means that, e.g. "dot point" lists or poems with deliberately short lines will be displayed correctly without the author having to do any extra work or the client having to be any smarter in order to recognise and handle that kind of content corectly.

For most "everyday" writing, this approach means you probably just want to use one line per paragraph.

Note that blank lines are rendered verbatim by clients verbatim, i.e. if you put two or three blank lines between your paragraphs, the reader will see two or three blank lines.

## Links

Like Gopher (and unlike Markdown or HTML), Gemtext only lets you put links to other documents on a line of their own.  You can't make a single word in the middle of a sentence into a link.  This takes a little getting used to, but it means that links are extremely easy to find, and clients can style them differently (e.g. to make it clear which protocol they use, or to display the domain name to help users decide whether they want to follow them or not) without interfering with the readability of your actual textual content.

Link lines look like this:

```
=&gt; https://example.com    A cool website
=&gt; gopher://example.com   An even cooler gopherhole
=&gt; gemini://example.com   A supremely cool Gemini capsule
=&gt; sftp://example.com
```

That is:

* They start with the two characters =&gt;,
* followed by optional whitespace (spaces or tabs, as many or as few as you like),
* followed by a URL (any protocol you like).
* They can end right there if you like, as per the sftp example above!
* Or they can be followed by at least one space or tab,
* And then a human-friendly label, which can be as long as you like

In the above example, all the URLs and labels lined up nicely because the author was pedantic.  But Gemini doesn't care, and this is fine too:

```
=&gt;https://example.com A cool website
=&gt;gopher://example.com      An even cooler gopherhole
=&gt; gemini://example.com A supremely cool Gemini capsule
=&gt;   sftp://example.com
```

## Headings

Gemtext supports three levels of headings.  Headings are limited to a single line and start with either one, two or three # symbols followed by one mandatory space character:

```
# Heading

## Sub-heading

### Sub-sub-heading
```

This is the only supported heading syntax.  Underlining your headings using - or = symbols like in Markdown will not do anything.

It is strictly optional for clients to do anything special at all with headings.  Many clients will recognise them and use a larger font or a different colour or some other kind of styling, but some will not and will just treat them as ordinary text lines and print them as is.  This is fine, because headings are not supposed to be used to control the appearance of your content.  Rather, they supply important semantic information on the structure of your content.  Some clients will use headings to automatically generate a table of contents for their user, which can be useful for navigating large documents.  Software for generating Atom or RSS feeds might use headings to automatically detect the titles for gemlog posts.

## Lists

Gemtext supports unordered lists.  Each item in a list is written as a single long line, which begins with a single * symbol followed by one mandatory space character:

```
* Mercury
* Gemini
* Apollo
```

This is the only supported list syntax.  Using - instead of * like in Markdown will not do anything.  Nested lists are not supported.

It is strictly optional for clients to do anything special at all with list items, and some clients will treat them just like any other line of text.  The only reason they are defined is so that more advanced clients can replace the * with a nicer looking bullet symbol and so when list items which are too long to fit on the device screen get broken across multiple lines, the lines after the first one can be offset from the margin by the same amount of space as the bullet symbol.  It's just a typographic nicety.

## Blockquotes

Gemtext supports blockquotes.  The quoted content is written as a single long line, which begins with a single &gt; character:

```
&gt; Gemtext supports blockquotes.  The quoted content is written as a single long line, which begins with a single &gt; character
```

It is strictly optional for clients to do anything special at all with blockquotes, and some clients will treat them just like any other line of text.  As per list items, they are defined strictly to allow for more pleasant typography in ambitious clients.

## Preformatted text

Gemtext is carefully designed to be very, very easy to parse and render.  Gemini clients process Gemtext one line at a time, rendering each line independently of any lines before it or after it, just by peeking at the first few characters of a line to check for something like =&gt;, # , * , etc.

A line which starts with ``` (i.e. with three backticks) tells the client to toggle between its ordinary parsing mode, and "preformatted mode".  In preformatted mode, clients don't check whether or not a line is a link or a heading or anything else.  They are simply printed as-is.  Also, while clients may use variable width fonts for ordinary all other text, in preformatted mode clients must use a fixed width font.  Thus, a pair of ``` lines acts much like &lt;pre&gt; and &lt;/pre&gt; tags in HTML.

Preformatted text can be used to include ASCII art, source code, or similar content in a Gemtext document without clients mistakenly interpreting lines as headings, list items, etc.  It can also be used to write documents like this one, which explain Gemtext syntax with examples - you're able to see the syntax examples above without your client interpreting them like it normally would because they are rendered in preformatted mode.

Anything which comes after the ``` characters of a line which toggles preformatted line *on* (i.e. the first, third, fifth, etc. toggling lines in a document) may be treated as "alt text" for the preformatted content.  In general you should not count on this content being visible to the user but, for example, search engines may index it and screen readers may read it to users to help the user decide whether the preformatted content should be read aloud (which e.g. ASCII art generally should not be, but which source code perhaps should be).  There are currently no established conventions on how alt text should be formatted.
</pre>
</main>
<script src="find-fragment.js"></script>
<script src="link-to-here.js"></script>
</html>
