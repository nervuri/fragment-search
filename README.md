# URI fragment search

https://nervuri.gitlab.io/fragment-search/

Demo for using the URI fragment (the part after "#") as input for searching text in the current page. The syntax is: `#<text>[:<occurrence>]` - where `<text>` is a URI-encoded excerpt from the document and `<occurrence>` is an optional integer.

This demo is [intended for Gemini](https://lists.orbitalfox.eu/archives/gemini/2020/004369.html) - also see [the AV-98 fork](https://tildegit.org/nervuri/AV-98) with experimental frgament support.

As for the Web, the idea [has been around](https://indieweb.org/fragmention) for a long time and a standard for it is being discussed:

* https://wicg.github.io/scroll-to-text-fragment/
* https://chromestatus.com/feature/4733392803332096
* https://github.com/WICG/scroll-to-text-fragment
* https://temp.treora.com/text-fragments-ts/demo.html

Chromium already supports it and there is [a browser extension](https://chrome.google.com/webstore/detail/link-to-text-fragment/pbcodcjpfjdpcineamnnmbkkmkdpajjg/) for generating links to selected text.

I do not recommend using this demo's code on any website.  It was not written with portability in mind.  It relies on the non-standard [window.find()](https://developer.mozilla.org/en-US/docs/Web/API/Window/find) function, so it may not behave correctly in all browsers.  The "link to here" option does not work on mobile devices.
